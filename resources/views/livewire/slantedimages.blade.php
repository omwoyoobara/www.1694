<div class="w-full   h-auto overflow-x-visible ">
    
        <!-- Slanted Images Section -->
            <div class="flex space-between align-center flex-row space-y-4 max-w-screen-xl  h-900 p-9 w-full relative left-1/2 -translate-x-1/2">
                <div class="w-80 rotate-6  rounded-xl w-screen max-w-screen-xl mx-auto  bg-red-400 mr-7">
                    <img src="{{ asset('images/images/photos/image-1.jpg')}}" alt="Image 1" class="w-500 h-full object-cover rounded-xl mx-auto block max-w-full">
                </div>
                <div class="w-80 rotate-6 rounded-xl w-screen max-w-screen-xl mx-auto bg-red-400 mr-7">
                    <img src="{{asset('images/images/photos/image-2.jpg')}}" alt="Image 2" class="w-full h-full object-cover rounded-xl mx-auto block  max-w-full">
                </div>
                <div class="w-80 bg-red-400 -rotate-6 rounded-xl w-screen max-w-screen-xl bg-red-400 mx-auto mr-7">
                    <img src="{{ asset('images/images/photos/image-3.jpg') }}" alt="Image 3" class="w-full h-full object-cover rounded-xl mx-auto block max-w-full">
                </div>
                <div class="w-80 rotate-6  border-2xl rounded-xl w-screen max-w-screen-xl bg-red-400 mx-auto mr-7">
                    <img src="{{ asset('images/images/photos/image-4.jpg')}}" alt="Image 4" class="w-full h-full object-cover rounded-xl mx-auto block max-w-full">
                </div>
                <div class="w-80  -rotate-6 rounded-xl w-screen max-w-screen-xl mx-auto bg-red-400 mr-7">
                    <img src="{{ asset('images/images/photos/image-5.jpg')}}" alt="Image 5" class="w-full h-full object-cover rounded-xl mx-auto block max-w-full">
                </div>
            </div>
        
    
</div>
