<div class="mt-14 ">
    <div class="border-none  ">
        <div class=" flex   space-x-60 items-center">
            <h3 class="font-bold text-sm">Knowledge Base</h3>
            <div>
                <p class="text-sm text-centre text-zinc-400 dark:text-zinc-500 flex-start ">Preparing for the workshop</p>
            </div>

        </div>


        <div class="ml-48 mt-10">

            <a href="https://www.youtube.com/watch?v=UMUztLQNqSI " class="mb-10 bg-red-800" target="_blank">
                <div class="hover:bg-slate-100 w-auto p-7">
                    <div class="flex items-center ">
                        <div class="bg-slate-400 w-px h-3"></div>
                        <p class="ml-3 text-gray-500 ">
                            Cardano Basics
                        </p>
                    </div>
                    <h3 class="max-w-12 mt-4">What is Cardano and Blockchain?</h3>
                    <p class="max-w-12 mt-4">"Are you new to blockchain or Cardano, here is a good primer before attending the workshop."</p>
                    <a href="https://www.youtube.com/watch?v=UMUztLQNqSI" target="_blank" class="text-emerald-400 mt-4"> Watch video <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </a>

            <a href="https://www.youtube.com/watch?v=fe9XvezpdbI " class="mb-10 bg-red-800" target="_blank">
                <div class="hover:bg-slate-100 w-auto p-7">
                    <div class="flex items-center ">
                        <div class="bg-slate-400 w-px h-3"></div>
                        <p class="ml-3 text-gray-500 ">
                        CIPs & CIP 1694
                        </p>
                    </div>
                    <h3 class="max-w-12 mt-4">What is a CIP and what CIP 1694 all about?</h3>
                    <p class="max-w-12 mt-4">"The be the most helpful and egaged with with the workshop we recommend you read the entire CIP here on the homepage. If this is all brand new to you, here's a video give you a 10 thousand foot view before diving in."</p>
                    <a href="https://www.youtube.com/watch?v=fe9XvezpdbI" target="_blank" class="text-emerald-400 mt-4"> Watch video <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </a>


            <a href="https://www.1694.io/_next/static/media/iog-cip-in-a-nutshell.d74491b5.png " class="mb-10 bg-red-800" target="_blank">
                <div class="hover:bg-slate-100 w-98 p-7">
                    <div class="flex items-center ">
                        <div class="bg-slate-400 w-px h-3"></div>
                        <p class="ml-3 text-gray-500 ">
                        Infographic
                        </p>
                    </div>
                    <h3 class="max-w-12 mt-4">CIP-1694 IN A NUTSHELL</h3>
                    <p class="max-w-12 mt-4">A short infographic introduction to CIP-1694 by IOG and able-pool.io</p>
                    <a href="https://www.1694.io/_next/static/media/iog-cip-in-a-nutshell.d74491b5.png" target="_blank" class="text-emerald-400 mt-4">Open Diagram <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </a>

        </div>


    </div>

</div>



