<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        The CIP-50 analysis of actual state of Cardano network decentralization has taught one very powerful lesson so far: High leverage is bad.
        Applying this lesson to social decentralization and governance it would be wise to limit the voting power of all SPO's to some leverage limit multiple of the SPO's pledge: L. This has a profound effect: there is an added social/governance motivation for pledge. Groups like Binance and Coinbase which do not pledge any ADA would have no voting power.
    </p>



    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-27T17:18:28Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1328298447" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>