<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>
    <div style="overflow-x: auto; white-space: nowrap;">
        <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
            <p>
                I do not believe that a governance model for <br> Cardano can work without fiduciary obligation.<br> The draft CIP it Takes a Village, for example,<br> seeks to propose a model of government for<br> Cardano that does not aim to introduce a <br>systemic denial or avoidance of fiduciary <br>responsibility into the Cardano ecosystem.
            </p>

        </div>
        <p class="mt-5">
            The specification section of that CIP looks to be a list of questions <br>, not a concrete proposal <br>: https://github.com/paradoxicalsphere/cardano-improvement-<br>proposals/blob/7ebe042ca104eb15c679ebab16148c065d1639f0/CIP-<br>x/README.md#specification
        </p>
        <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
                2022-12-02T16:04:28Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1335473269" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
        <a href="https://github.com/JaredCorduan" target="_blank" class="flex items-center mb-6">
            <img src="{{asset('images/images/logos/conversation4.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
            <h1 class="ml-4">JaredCorduan</h1>
        </a>
    </div>
    <p class="mt-5">
        Hi @JaredCorduan <br>
        Yes, true, it's a starting point awaiting significant input from others who see the potential of the idea.
        Further discussion is at #voltaire-governance (server invite) Your thoughts and comments would be more than welcome. I'm open to questions and constructive criticism on how to best develop the idea. I don't necessarily see the work that you're doing here as somehow mutually exclusive of my draft, so far. I don't see why the two proposals could not co-exist, potentially. You have a great deal of skill and talent here. I hope that you may value my input as well.
    </p>
    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-02T16:10:38Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1335484463" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr class="mt-5">

    <div style="overflow-x: auto; white-space: nowrap;">

        <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
            <p>
                @paradoxicalsphere : [...] somehow mutually <br>exclusive of my draft, so far. I don't see why the <br>two >proposals could not co-exist, potentially.
            </p>
        </div>

        <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
            <p>
                https://discord.com/channels/97178511077083<br>1360/1047472328566657084/1048148685311<br>184976 @KtorZ : There's a reason why CIPs are<br> on Github and are purposely geared towards <br>developers / software engineers. CIPs offer a <br>way to discussion technical evolutions of the <br>network. They aren't about how IO, CF or<br> Emurgo manage their respective entities. If <br>there are suggestions about how the CF should <br>manage their funds and what their internal <br>policies should be then CIPs are certainly not<br> the right place to discuss that.
                This is also true <br>of this channel which is meant to discuss <br>CIP-1694 primarily, and more broadly, <br>what on-chain mechanisms can be put in place <br>to support Cardano's governance.
                So please, <br>I'll ask folks here (mainly @paradoxicalsphere) <br>to stop using this channel to discuss matters <br>that are clearly out of scope of the CIP process. <br>Otherwise, we'll just close this and invite you to <br>have this conversation elsewhere. There's also <br>no point submitting such a CIP because, as I <br>said, this is completely out of the scope of the CIP process.
            </p>
        </div>
        <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
                2022-12-02T16:23:32Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1335499485" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
        <a href="https://github.com/michael-liesenfelt" target="_blank" class="flex items-center mb-6">
            <img src="{{asset('images/images/logos/conversation2.png')}}" alt="Image 2" class="w-12 h-12 rounded-full">
            <h1 class="ml-4">michael-liesenfelt</h1>
        </a>
    </div>
    <p class="mt-5">
        Hi, <br>
        My reason for participating in the current mega-thread is encouragement to do so from https://github.com/cardano-foundation/CIPs/issues/376#issuecomment-1322688823 and a commitment to following the CIP process.
    </p>
    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-02T20:35:04Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1335816378" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>
</div>