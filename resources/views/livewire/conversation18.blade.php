<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>

    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <div class="mt-2 bg-slate-100 h 20 p-2 border-2  border-l-slate-400">
            <p>
                Applying this lesson to social decentralization and governance it would be wise to limit the voting power of all SPO's to some leverage limit multiple of the SPO's pledge: L. This has a profound effect: there is an added social/governance motivation for pledge. Groups like Binance and Coinbase which do not pledge any ADA would have no voting power.
            </p>

        </div>
        <p class="mt-5">
            I think this is a very valid point, thank you @michael-liesenfelt . How do we reconcile this, however, with the fact that SPOs that do not wish for a hard fork can always just not upgrade their software? If they do that, as a collective, then the 'no' vote is basically determined by the longest chain, which ignores pledge.
        </p>
    </div>
    <p class="mt-3">
        Need to allow for the continual fragmentation of groups. Within SPO's you need to have a mechanism that allows them to split from the main vote and give their vote elsewhere. This is assuming you have a yes or no proposal system rather than one that allows fragmentation of yes or no options.
    </p>
    <p class="mt-2">
        Idk how to reply to these things so forgive me if I did things wrong but wanted to make that point.
    </p>

    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-03T20:13:45Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1336250269" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>
</div>