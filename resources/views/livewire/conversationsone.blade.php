<div class="mt-9">
    <a href="https://github.com/Kronoshus" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation1.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">Kronoshus</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl ">
        Overall I'm very impressed with and happy with the contents, structure, ideas, and work put into the draft CIP-1694. Personally I was considering some similar ideas for governance, however because this isn't my field of expertise my ideas were not nearly as comprehensive.
        One feature of this CIP that I really like is that there is no mandatory hierarchical structure for how payouts must be made. This leaves funding the future community structure of the (members based organization, professional society, Catalyst startup incubator, whatever outcome of the Cardano constitutional process) entirely flexible. This flexibility is wise and is absolutely necessary for financial (fund/defund) checks-and-balances of future Cardano (organizations/ societies/ companies/ developers/ contractors/ ect).
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-21T15:05:01Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1322201219" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>