<div class="mt-9">
    <a href="https://github.com/michael-liesenfelt" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation2.png')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">michael-liesenfelt</h1>
    </a>
    <hr>

    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400 max-w-sx">
        <p class="sm:max-w-2xl">
        One feature of this CIP that I really like is that there is no mandatory hierarchical structure for how payouts must be made. This leaves funding the future community structure of the (members based organization, professional society, Catalyst startup incubator, whatever outcome of the Cardano constitutional process) entirely flexible. This flexibility is wise and is absolutely necessary for financial (fund/defund) checks-and-balances of future Cardano (organizations/ societies/ companies/ developers/ contractors/ ect).
        </p>
    </div>

    <p class="mt-4">
    Thank you, Michael. Yes, the CIP is deliberately foundational rather than prescribing any specific structure.
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-21T16:39:58Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1322345820" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

    

</div>