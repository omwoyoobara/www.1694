<div class="pt-10 sm:pt-20 pl-5 ">
    <h3 class="text-2xl sm:text-middle font-medium pb-2">Community Translations</h3>
    <hr>
    <!--Links for community translation-->
    <div class="mt-5">
        <a href="https://docs.google.com/document/d/1pMPifPW2gcFSJ82bMN1HqHJnsk7jM0G5qqf2LaiuXmo/edit?usp=sharing" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pt-1 pb-2 sm:text-small">Arabic</div>
        </a>
        <a href="https://docs.google.com/document/d/1kq_VCmiQDv-Pncg67LRmX5kWSB_pmW62JRioo-Djh60/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer  pb-2 sm:text-small">Dutch - Google Doc</div>
        </a>
        <a href="https://forum.cardano.org/t/cip-1694-een-on-chain-gedecentraliseerd-bestuursmechanisme-voor-voltaire/113402" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Dutch - Cardano Forum</div>
        </a>
        <a href="https://docs.google.com/document/d/170k-HUid8mI0WaoOfTgSi2FbTQ5RS_1xOw0ZF9AyXdM/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">German</div>
        </a>
        <a href="https://docs.google.com/document/d/1OJaVdvWsvuaTldMsPgEMTCbmsjaFuHcrrtYivVFmB9o/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">French</div>
        </a>
        <a href="https://docs.google.com/document/d/182h4TUHQhrnhn-7NJ_Tez7BaPOKmovkF5WWJi73bkRg/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Japanese</div>
        </a>
        <a href="https://oscarwest.notion.site/CIP-1694-f9ec6e269a3f43b1922e3334114d102c" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Korean</div>
        </a>
        <a href="https://docs.google.com/document/d/188qxKaTLJX0fIrzJpE_TX_5WIc3bcIJMzRC9x3nIRFc/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Indonesian</div>
        </a>
        <a href="https://docs.google.com/document/d/1xgykELMz7KH-zY9Iv69XwWWfTmrGUmFONVh-ZH6kIXo/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Portuguese</div>
        </a>
        <a href="https://docs.google.com/document/d/1TKJwtpjGiTibJbFv7LlCWTw5qDTzGxqzKAN9QO7EXvo/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Spanish</div>
        </a>
        <a href="https://docs.google.com/document/d/1OWz-zywtgmB9U4ObmuAzQCrC-6bor97j0RIYHXavpYM/edit" target="_blank" class="text-orange font-semibold text-xl text-rose-400 hover:text-rose-700">
            <div class="cursor-pointer pb-2 sm:text-small">Swahili</div>
        </a>

    </div>
    <p class="mt-2 sm:text-smaller">
        Translations done through rewarded bounties at the Catalyst Swarm Bounty Board
        Donations to Catalyst Swarm:
    </p>
    <p class="max-w-xs">
        <b>
            Addr1qxhxg0mwzahfv8x4nr5s9zmffssxueqsnxx<br>v282kz2c30nykg8fw8x99crukwyc7yftwfgxmhsu<br>2xx0n8elfvj7mljlqm45kgs
        </b>
    </p>

</div>