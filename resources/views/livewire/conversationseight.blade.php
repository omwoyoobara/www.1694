<div class="mt-9">
    <a href="https://github.com/Kronoshus" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation1.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Kronoshus</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        In reference to Juan's 3rd point here, https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1327335051
        He is right. There is no legal procedure to defend oneself if being removed from a committee, thus what incentive would anyone have to do anything once on the committee besides hold power.
        V/r,
        Kevin Mohr
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-26T16:53:50Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1328079164" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>

