<div class="mt-9">
    <a href="https://github.com/Balance-Analytics" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation12.jpeg')}}" alt="Image 12" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Balance-Analytics</h1>
    </a>
    <hr>
    <div style="overflow-x: auto; white-space: nowrap;">
        <div class="bg-slate-100 border-4 border-l-slate-400" style="display: inline-block;">
            <div class="mt-4 p-2 ml-5">
                <p>
                    2. I was thinking about the point 1 and another idea comes to me. Maybe we need an initial Constitutional Committee which creates the constitution and has a grace period to govern with its rules. After this grace period, we would need to revalidate all the members and/or add new ones for the first "Governance Committee".
                </p>
                <p class="mt-5">
                    3. I understand that we can remove members of the CC with an action proposal, but it requires someone to do actively an action. I'm not comfortable with this situation because this doesn't incentivize the committee members to participate, and it might cause that they prefer to have a passive role. I think that the membership of the committee should be revalidated after some time.
                </p>
            </div>


        </div>
        <p style="white-space: nowrap;" class="mt-5">yep, these are valid concerns. I think a forced election after the first Constitutional Committee is less controversial. If we forced a re-election at timed intervals, the worry is that progress grinds to a halt (but could be worthwhile anyway).</p>


        <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
                2022-11-28T19:08:50Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1329610192" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
        <a href="https://github.com/JaredCorduan" target="_blank" class="flex items-center mb-6">
            <img src="{{asset('images/images/logos/conversation4.jpeg')}}" alt="Image 12" class="w-12 h-12 rounded-full">
            <h1 class="ml-4">
                Jared Corduan
            </h1>
        </a>
    </div>

    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400 sm:max-w-2xl">
        <p>
            Applying this lesson to social decentralization and governance it would be wise to limit the voting power of all SPO's to some leverage limit multiple of the SPO's pledge: L. This has a profound effect: there is an added social/governance motivation for pledge. Groups like Binance and Coinbase which do not pledge any ADA would have no voting power.
        </p>
    </div>

    <p class="mt-5">
        I think this is a very valid point, thank you @michael-liesenfelt . How do we reconcile this, however, with the fact that SPOs that do not wish for a hard fork can always just not upgrade their software? If they do that, as a collective, then the 'no' vote is basically determined by the longest chain, which ignores pledge.
    </p>

    <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
            2022-11-28T19:13:40Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1329616811" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
</div>