<div class="flex justify-between items-center p-4 flex-col md:flex-row md:space-x-4 mb-4 ml-4 ">
  
  <div class="flex space-x-4 whitespace-nowrap sm:pb-10">
    <a href="https://www.lidonation.com/en" target="_blank" class="hover:text-cyan-400">Lido Nation</a>
    <a href="https://www.lidonation.com/en/catalyst-explorer/proposals" target="_blank" class="hover:text-cyan-400">Catalyst Explorer</a>
    <a href="https://cips.cardano.org/" target="_blank" class="hover:text-cyan-400">Cardano CIPs</a>
  </div>

  <div class="ml-20 whitespace-nowrap sm:ml-5">
    <p class="text-gray-500 sm:text-small">&copy;2023 A LIDO Nation Project</p>
  </div>

</div>
