<div class="mt-9">
    <a href="https://github.com/Kronoshus" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation1.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Kronoshus</h1>
    </a>
    <hr>
    <div style="overflow-x: auto; white-space: nowrap;" class="pb-3">
        <p>I saw an interesting video by army of spies about chat.openai.com. I was <br>inspired by the platforms outcome when asked to write a 4 paragraph <br>academic essay comparing and contrasting the theories of nationalism of<br> Benedit Anderson and Ernest Geller. In 10 seconds a very decent essay <br>popped out.</p>

        <p>
            I think the huge legacy of human endeavour to today contains all the wisdom <br>to construct a very inspiring Constitution.<br>
            If this AI was asked to list the top 12 points for Human prosperity from the <br>Zoroastrian Avesta, The Hindu Ramayana and Mahabharata, The Buddhist <br> Tipitaka,, The Bible, The Koran The Tora etc, plus as many Legal rights <br>and Constitutions of the world, US , Magna Carta etc<br>Reinventing the wheel is not required. AI's impartial unbiased, non sectarian <br>and very fast results might give us something very balanced and valuable<br> framework to draw upon.
        </p>

        <p class="mt-3">
            From: Jared Corduan <b> @.> Sent: Saturday, December 3, 2022 12:23:59 AM To: <br> cardano-foundation/CIPs @.> </b>Cc: Paul Morris @.>; Mention @.> Subject: Re:<br> [cardano-foundation/CIPs] CIP-1694? | A proposal for entering the Voltaire <br> phase (PR #380) <br>
            <hr>
            @JaredCorduan commented on this pull request.
        </p>

        <p class="mt-3">
            In CIP-1694/README.mdhttps://github.com/cardano-<br>foundation/CIPs/pull/380#discussion_r1038312060:
        </p>


        <ul class="list-disc mt-4">
            <li>
                <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
                    <p>
                        Otherwise, SPO vote endorsement is required. <br>The :x: symbol indicates that the AVST will be <br> ignored, and only the DRep and SPO votes will <br>be considered regardless of the AVST.
                    </p>
                </div>
            </li>
            <li>
                +* SPOs<br />
                The SPO vote threshold which must be met as a percentage of the stake <br>held by all stake pools. The SPO vote is only considered if the AVST<br> threshold is :x: or the AVST is below the AVST threshold.
            </li>

            <li>
                +| Governance Action Type | Constitutional Committee | DReps | AVST | SPOs |<br> +| --- | :---: | --- | --- | --- | +| <br>1. Motion of no-confidence | :x: | $P_1$ | :x: | $R_1$ | +| 2(a). New Committee/quorum (normal state) | :heavy_check_mark: | $P_{2a}$ <br>| $Q_{2a}$ | $R_{2a}$ | +| 2(b). New Committee/quorum (state of no-confidence)|<br> :x: | $P_{2b}$ | :x: | $R_{2b}$ | +| 3. Update to the Constitution | <br>:heavy_check_mark: | $P_3$ | $Q_3$ | $R_3$ | +| 4. Hard-Fork initiation | <br>:heavy_check_mark: | $P_4$ | :x: | $R_4$ | +| 5. Protocol parameter changes |<br> :heavy_check_mark: | $P_5$ | $Q_5$ | $R_5$ | +| 6(a). Treasury withdrawal,<br> $[T_0, T_1)$ | :heavy_check_mark: | $P_{6a}$ | $Q_{6a}$ | $R_{6a}$ | +| 6(b).<br> Treasury withdrawal, $[T_1, T_2)$ | :heavy_check_mark: | $P_{6b}$ | <br>$Q_{6b}$ | $R_{6b}$ | +| 6(c). Treasury withdrawal, $[T_2, T_3)$ | :heavy_check_mark: | <br>$P_{6c}$ | $Q_{6c}$ | $R_{6c}$ |<br>
                I have not yet added anything quite as prescriptive as what you are <br>suggesting, but made a note of the topic in general:<br>
                https://input-output-<br>rnd.slack.com/archives/GJGPC1QA3/p1669973606029659?<br>thread_ts=1669973559.536239&cid=GJGPC1QA3<br>
                There are a few interesting things to still think about with respect to this<br> action, and they might have interactions.<br>
                — Reply to this email directly, view it on GitHubhttps://github.com/cardano<br>-foundation/CIPs/pull/380#discussion_r1038312060, or <br>unsubscribehttps://github.com/notifications/unsubscribe-<br>auth/A4MSBKP7AOLRDOXCWAZLW2DWLIPB7ANCNFSM6AAAAAASD5YWY4.<br> You are receiving this because you were mentioned.Message ID: @.***>
            </li>

        </ul>

        <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
                2022-12-04T00:33:46Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1336287448" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>

        <a href="https://github.com/WhiteHorseAvatar" target="_blank" class="flex items-center mb-6">
            <img src="{{asset('images/images/logos/white.png')}}" alt="Image 2" class="w-12 h-12 rounded-full">
            <h1 class="ml-4">
                WhiteHorseAvatar</h1>
        </a>


    </div>

    <p class="mt-5">
        DReps is a role in Project Catalyst already with hundreds of people signed up. I fear that using the same term for this CIP will cause a fair amount of confusion. Is it possible to use a different name so we're not overloading DRep to represent two different roles?
    </p>

    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-04T19:54:48Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1336502768" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>


    <a href="https://github.com/xeeban" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/cartoon.png')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            xeeban</h1>
    </a>

    <hr>

    <p class="mt-5">
        Hi @xeeban, <br>
        Please also consider a governance-related CIP draft at an earlier stage of development having far fewer problems at https://change.paradoxicalsphere.com/cip
    </p>

    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-04T19:57:42Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1336503272" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>


</div>