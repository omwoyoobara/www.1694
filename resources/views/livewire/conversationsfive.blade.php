<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs">
        Hi all, <br>I've just read the CIP-1694 and the comments in this issue. I have some thoughts about them:
    </p>
    <ul class="list-decimal ml-10 mt-3">
        <li class="mb-2">I read some comments above which I agree with. I feel that the name of the Constitutional Committee isn't accurate. It should be named something like Governance Committee or something like this.</li>
        <li class="mb-2">I was thinking about the point 1 and another idea comes to me. Maybe we need an initial Constitutional Committee which creates the constitution and has a grace period to govern with its rules. After this grace period, we would need to revalidate all the members and/or add new ones for the first "Governance Committee".</li>
        <li class="mb-2">
        I understand that we can remove members of the CC with an action proposal, but it requires someone to do actively an action. I'm not comfortable with this situation because this doesn't incentivizes the committee members to participate, and it might cause that they prefer to have a passive role. I think that the membership of the committee should be revalidated after some time.
        </li>
    </ul>

    <p class="mt-5 max-w-xs">
     I hope these points are useful, or at least they spark some debate.Juan
    </p>
    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-25T11:03:26Z

        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1327335051" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>