<div class="mt-9">
    <a href="https://github.com/JaredCorduan" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation4.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">JaredCorduan</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        Hi @JaredCorduan et al,

        @ltouro suggested that I review your proposal. I’m interested in learning about your CIP. Could you please give me some links where I can read about the background for the idea?

        Specifically, I am interested in reading about your rationale for the model of government that you propose for Cardano. I began drafting a CIP myself that would involve a model of government based fundamentally on a decentralized autonomous organization (DAO). The draft is available at CIP-x?—I Gave at the Office. Comments are welcome at Cardano Governance #376.

        I understand some of the challenges associated with creating a stable DAO. For example, see Waves founder: DAOs will never work without fixing governance. I believe that my CIP draft maintains a starting point capable of successfully overcoming such challenges.

        I have a background in IT as a Technical Writer and Instructional Designer, with a Business Analysis foundation. My graduate research in clinical psychology focussed on investigating social networks within workplaces and organizations using electronic communications, and I hold a related US patent.

        In response to the concerns expressed by @Kronoshus regarding doing work for free, I believe that such resentment should not be ignored. Have you considered seeking funding for developing the CIP through Project Catalyst? I would be interested in exploring such an avenue myself. I believe that the timing may be good to apply for the next round of funding, with community voting set to start sometime early in 2023, if I recall correctly. I believe that the Project Catalyst application essentially boils down to an estimate and a schedule, which would be important documents to prepare and maintain for a successful project anyway, in my experience.

        Oliver
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-24T21:26:15Z

        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1326845097" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>