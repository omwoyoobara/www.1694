<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>
    <div style="overflow-x: auto; white-space: nowrap;" class="sm:wrap">
        <div class="" style="display: inline-block;">
            <p>Fair enough!</p>
            <div class="mt-4 p-2 ml-5">
                <p>
                    From: Jared Corduan <b> @.> Sent: Tuesday, 29 November 2022 9:32 PM To:</br> cardano-foundation/CIPs @.></b> Cc: Paul Morris @.>; Mention @.> Subject: Re: <br>[cardano-foundation/CIPs] CIP-1694? | A proposal for entering the Voltaire <br>phase (PR #380)
                </p>
                <p class="mt-5">
                    @JaredCorduan commented on this pull request. <br>
                    In CIP-1694/README.mdhttps://github.com/cardano <br>-foundation/CIPs/pull/380#discussion_r1034757533:
                </p>
                <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
                    <ul class="list-disc ml-10 ">
                        <li></li>
                    </ul>
                </div>
                <ul class="list-disc ml-10 sm:ml-0 ">
                    <p>+* The PPUP transition rule will be rewritten and moved out of the UTxO rule <br> and into the LEDGER rule as a new TALLY rule. +</p>
                    <li>It will process the governance actions and the votes, ratify them, and <br>stage governance actions for enactment in the current or next epoch, as <br> appropriate.</li>
                    <li><br>
                        +* The NEWEPOCH transition rule will be modified. +* The MIR sub-rule will <br> be removed. +* A new ENACTMENT rule will be called immediately after the <br> EPOCH rule. This rule will enact governance actions that have previously <br>been ratified. +* The EPOCH rule will no longer call the NEWPP sub-rule or <br> compute whether the quorum is met on the PPUP state.
                    </li>
                    <li><br>
                        +#### More on DRep incentives + +The DReps arguably need to be <br>compensated for their work.
                    </li>
                    <li class="sm:max-w-md"><br>
                        +The corresponding incentive mechanisms need to be specified, with the <br> funds probably coming from the per-epoch treasury allocation. <br> +Performance constraints will also need to be considered since it would be <br> problematic if millions of DReps were expected to vote on each governance <br> action. Some incentive options to ensure a manageable number of DReps <br>include: <br>
                        i would hope that the requirement to vote is rare <br>
                        Some of the actions we expect to be rare, such as changing the constitution. <br>
                        Some of the actions we expect to be common, such as treasury withdrawals for things like Catalyst. <br>
                        Some of the actions could really go either way, there are valid arguments <br> that some of the protocol parameters should be more adaptive. <br>
                        — Reply to this email directly, view it on GitHubhttps://github.com/cardano-<br>foundation/CIPs/pull/380#discussion_r1034757533, or <br> unsubscribehttps://github.com/notifications/unsubscribe-<br>auth/A4MSBKKJ6YN6HZXT4GBZ2CLWKYAX3ANCNFSM6AAAAAASD5YWY4. <br> You are receiving this because you were mentioned.Message ID: @.***>
                    </li>
                </ul>

            </div>


        </div>
        <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
                2022-11-29T13:44:16Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1330673476" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
        <a href="https://github.com/WhiteHorseAvatar" target="_blank" class="flex items-center mb-6">
            <img src="{{asset('images/images/logos/white.png')}}" alt="Image 2" class="w-12 h-12 rounded-full">
            <h1 class="ml-4">WhiteHorseAvatar</h1>
        </a>

    </div>
    <div style="overflow-x: auto; white-space: nowrap;" class="pb-3">
        <p class="mt-5 ">
            To encourage community involvement while also keeping the <br>discussion here on GitHub confined to the technical proposal and <br>its documentation, we've resolved at the CIP Editors' Meeting <br> today to create a Discord channel for more liberal discussion: <br>#voltaire-governance (server invite)
        </p>

        <div class="mt-4 bg-slate-100  p-5 border-4  border-l-slate-400 sm:max">
            <p>
                Voltaire entry & governance discussions - For <br>observations and feedback around the pending <br>CIP-1694 (https://github.com/cardano-<br>foundation/CIPs/pull/380) and other CIPs which <br>may relate to the technical aspects of Cardano <br> on-chain governance.

                Please note the <br>distinction between governance as applied to <br>blockchain systems (impartial systems of <br> assigning influence and control by consensus) <br>and government established by conventional <br>legal or political systems. To keep discussion <br>technically relevant, a list of "on" topics <br>will be posted here when available.

                Resolutions <br> from & summaries of the discussion here, if <br>relevant to the CIP process (https://github.com/<br>cardano-foundation/CIPs/blob/master/CIP-0001)<br>, should be posted back to the corresponding <br>GitHub public CIP discussion thread. The Editors <br>are not obligated to summarise nor add any<br> comments from other users to the relevant <br>GitHub PR discussion threads.
            </p>

        </div>
        <p class="mt-4">
            Note this follows suit with the existing Discord group created for <br> https://github.com/cardano-foundation/CIPs/pull/242 - but also <br>that there is also a Governance discussion emerging here (which<br> may be additionally useful to participants who can't use Discord <br>): https://app.element.io/#/room/!nrkKgEbuxqnXktPqun:matrix.org<br>
            cc @JaredCorduan @michael-liesenfelt @KtorZ<br>
            (p.s. edited Mon 5 Dec 2022 15:45:05 UTC with information for <br>"rebooted" Discord channel)
        </p>

        <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
                2022-11-30T11:40:09Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1332019838" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
        <a href="https://github.com/rphair" target="_blank" class="flex items-center mb-6">
            <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
            <h1 class="ml-4">rphair</h1>
        </a>
    </div>
    <br>
    <p>
        Hi, <br>
        Further to my detailed comment on November 28, CIP 1694? seems to focus on leveraging financial stake in Cardano while specifically seeking to avoid or even prevent fiduciary responsibility. Fiduciary responsibility refers to a relationship in which one party (the fiduciary) is responsible for looking after the best interests of another party (the beneficiary). Fiduciary responsibility is a duty of loyalty and impartiality in placing the interests of a corporation, organization or individual first, not allowing decision making to be tainted by self-interest or self-dealing.
        I do not believe that a governance model for Cardano can work without fiduciary obligation. The draft CIP It Takes a Village, for example, seeks to propose a model of government for Cardano that does not aim to introduce a systemic denial or avoidance of fiduciary responsibility into the Cardano ecosystem.
    </p>
    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-02T15:51:48Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1335453079" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>
</div>