<div class="mt-9">
    <a href="https://github.com/kevinhammond" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation3.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">KevinHammond</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        thanks for all the great suggestions @michaelpj , I've just added a commit, "address comments 20221122", addressing a lot of it. In particular, I'm really glad that you suggested the rationale on the table values, as I found a problem. The AVST was not supposed to be ignored when the constitutional committee does not vote, it is required (which is stronger than diverting the responsibility to the SPOs). So now there is a new column in the table, and explanations.
        I've added several section to the rationale.
        I've left some TODO's, one for the potential other acceptance criterion and one to think more about the treasury voting thresholds (taking @michael-liesenfelt 's comments into account).
        I removed the yes/no ratio remarks that were not correct.
        I did not yet address any of the comments about the described implementation plan, I will probably need more help understanding those remarks.
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-23T01:18:58Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1324429745" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>