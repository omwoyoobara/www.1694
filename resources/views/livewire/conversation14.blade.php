<div>
    <a href="https://github.com/JaredCorduan" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation4.jpeg')}}" alt="Image 12" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Jared Corduan
        </h1>
    </a>
    <hr>
    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400 p-5">
        <div class="bg-slate-100 h 20 p-5 border-4  border-l-slate-400 ">
            <p>@michael-liesenfelt : limit the voting power of all SPO's to some leverage limit multiple of the SPO's pledge: L.</p>
        </div>
    </div>
    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            @JaredCorduan : How do we reconcile this, however, with the fact that SPOs that do not wish for a hard fork can always just not upgrade their software?
         </p>
    </div>

    <p class="mt-5">
    Naturally the result of CIP-50 with a leverage limit L would have to be hard-fork adopted and actively applied to block allocation schedules and reward incentives before it was also used for governance.
    </p>

    <div class="flex items-center mt-6 mb-5">
            <p class="text-zinc-400 text-sm">
            2022-11-28T21:15:01Z
            </p>
            <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1329772192" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
        </div>
</div>