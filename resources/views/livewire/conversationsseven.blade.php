<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        Thank you Oliver.

        I have considered asking for funding yet I still would like some sort of official support by the community or IOG. There are many that do not see the point in creating legal framework and I would like to hear from them because I only ever hear from people that agree with me. If everyone agrees a need for law is necessary then I will go ahead with making a full manual for tribunals.

        In addition to the need for punitive legal framework in a governance system, someone will also need to make administrative/business law for non-punitive governance actions such as all these nice ideas we love talking about. I am not very good at making legal code around that but a couple of lawyers have reached out to me for that. They just need to have official support from some entity too before writing the code.

        If law is not created before the governance proposal is implemented, then there will be many issues in the future or people simply will not use the system, call it broken like what happened with Catalyst, and move on with their lives.

        V/r,

        Kevin Mohr
    </p>



    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-26T16:48:25Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1328078360" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>