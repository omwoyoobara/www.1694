<div class="mt-9">
    <a href="https://github.com/JaredCorduan" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation4.jpeg')}}" alt="Image 12" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Jared Corduan
        </h1>
    </a>
    <hr>
    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            On the topic of pledge and leverage, would it be possible to include time duration for pledge existence? The use of time duration for a pools pledge (i.e. 10 epochs) could prevent pools from updating their pledge to increase vote impact then remove the pledge to keep their ADA in a more liquid state.
        </p>
    </div>
    <p class="mt-5">
        That's very interesting. Perhaps that could be a separate CIP.
    </p>



    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-11-28T19:15:37Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1329619415" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

    <!--2-->
    <a href="https://github.com/JaredCorduan" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation4.jpeg')}}" alt="Image 12" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Jared Corduan
        </h1>
    </a>
    <hr>
    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            I’m interested in learning about your CIP. Could you please give me some links where I can read about the background for the idea?
        </p>
    </div>
    <div class="mt-10 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            Is there a documented business case for CIP-1694? If so, could someone please post the link? I would like to get up to speed as quickly as possible. </p>
    </div>
    <p class="mt-5">
        @paradoxicalsphere - @CharlesHoskinson has made a lot of great content about governance on his youtube channel. you might enjoy these (and can probably find many others like it):
    </p>
    <ul class="list-disc mt-4 ml-6">
        <li><a href="https://www.youtube.com/watch?v=3_wJOF4Y8gU">https://www.youtube.com/watch?v=3_wJOF4Y8gU</a></li>
        <li><a href="https://www.youtube.com/watch?v=7_zIzjWZHgg">https://www.youtube.com/watch?v=7_zIzjWZHgg</a></li>
        <li><a href="https://www.youtube.com/watch?v=zhFTO1jYjbk">https://www.youtube.com/watch?v=zhFTO1jYjbk</a></li>
        <li><a href="https://www.youtube.com/watch?v=Ofj9u6e1tUo">https://www.youtube.com/watch?v=Ofj9u6e1tUo</a></li>
        <li><a href="https://www.youtube.com/watch?v=w2bhIQdzeI4">https://www.youtube.com/watch?v=w2bhIQdzeI4</a></li>
    </ul>

    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-11-28T20:24:50Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1329720690" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>
</div>