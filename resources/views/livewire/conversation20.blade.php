<div class="mt-9">
    <a href="https://github.com/paradoxicalsphere" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation5.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">ParadoxicalSphere</h1>
    </a>
    <hr>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            DReps is a role in Project Catalyst already with hundreds of people signed up. I fear that using the same term for this CIP will cause a fair amount of confusion. Is it possible to use a different name so we're not overloading DRep to represent two different roles? </p>

    </div>
    <p>
        This is the same role I believe? It is not using the same name for 2 different roles right?
    </p>

    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-12-05T02:00:13Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1336625001" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

    <a href="https://github.com/Kronoshus" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation1.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Kronoshus</h1>
    </a>
    <hr>
</div>