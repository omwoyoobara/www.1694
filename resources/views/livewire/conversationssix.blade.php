<div class="mt-9">
    <a href="https://github.com/jmagan" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation6.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">jmagan</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        Is there a documented business case for CIP-1694? If so, could someone please post the link? I would like to get up to speed as quickly as possible.
        If information is still being gathered, such as competitive analysis or benchmark studies to compare the strengths and weaknesses of CPI-1694? with those of Cardano's competitors, then I would like to suggest reviewing the PivX DAO (see also Why and How the PIVX DAO Works, for example).
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
            2022-11-26T15:38:36Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1328067565" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>