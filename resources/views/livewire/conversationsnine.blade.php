<div class="mt-9">
    <a href="https://github.com/Kronoshus" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation1.jpeg')}}" alt="Image 2" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            Kronoshus</h1>
    </a>
    <hr>
    <p class="mt-5 max-w-xs sm:max-w-2xl">
        Thank you @Kronoshus I am going to spend time to read An On-Chain Decentralized Governance Mechanism for Voltaire in detail.
        <br>
        Oliver
    </p>

    <div class="flex items-center  mt-6">
        <p class="text-zinc-400 text-sm">
        2022-11-26T19:12:15Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1328099467" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>

</div>