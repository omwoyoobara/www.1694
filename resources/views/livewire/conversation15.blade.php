<div class="mt-9">
    <a href="https://github.com/michael-liesenfelt" target="_blank" class="flex items-center mb-6">
        <img src="{{asset('images/images/logos/conversation2.png')}}" alt="Image 12" class="w-12 h-12 rounded-full">
        <h1 class="ml-4">
            michael-liesenfelt
        </h1>
    </a>
    <hr>
    <p class="mt-4">
        Hi @JaredCorduan et al, Hi @CharlesHoskinson,I would like to offer a positive and constructive response to the following list of concerns raised in the past few days in response to CIP-1694?:
    </p>
    <hr>
    <p>@jmagan (posted on November 25, 2022)</p>

    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            Maybe we need an initial Constitutional Committee which creates the constitution and has a grace period to govern with its rules. After this grace period, we would need to revalidate all the members and/or add new ones for the first "Governance Committee".
        </p>
    </div>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            ...we can remove members of the CC with an action proposal, but it requires someone to do actively an action. I'm not comfortable with this situation because this doesn't incentivizes the committee members to participate, and it might cause that they prefer to have a passive role.
        </p>
    </div>
    <hr class="mt-2">
    <p>@michaelpj (posted on November 26, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            ...the logic of which properties you get for different vote and thresholds is quite complex and could stand to be spelled out extremely explicitly.
        </p>
    </div>
    <p class="mt-3"> @GGAlanSmithee (posted on November 26, 2022)</p>

    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            If anyone can vote (represent themselves) what is even the point with having representatives?
        </p>
    </div>

    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            ...another issue with other systems of representation (a democracy, for example) is that representatives get equal voting power on all issues that are presented before them (which would be all issues in the system described here, as I understand it). I see this as a bit problematic, since having a large stake of ADA does not automatically equates to having the right know how and understanding to make all types of decision.
        </p>
    </div>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            ...the system incentivizes active participation, which actually makes the prior point even worse. Representatives will be incentiviced to vote on issues they know little or nothing about. You could say that in such a case they should vote blank, but if so, why even include them in the vote at all? I think having some form of committee system would make sense here.
        </p>
    </div>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            This text doesn't describe what kind of forum (if any) there will be for the decisionmakers to get together, vet ideas, make their cases, argue points, etc.
        </p>
    </div>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            If there is no formal procedure/structure, as described in the prior point, so that conversations can happen openly, I fear that closed-room meetings will take their place (which is probably the case now to a large degree).
        </p>
    </div>
    <p class="mt-3">@GGAlanSmithee (posted on November 26, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            "I thought the whole point of DReps was that most votes should be delegated..." I think this is one of the most imporant issues in this text, that it seems to contradict itself in that it wants to incentivise dReps, but I think it will be hard for meaningful representation to happen if everyone represents themselves (for reasons previously stated).
        </p>
    </div>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            I do not like the idea of randomized subsets of the dReps, since that will really leave the door open for missrepreserntation imo (read earlier comments about this). I would be much more comfortable with dReps on specific classes of issues (read committees). In that case, the total set of representatives/votes/votes will not be as large for any given action/vote (since only a subset of dReps will vote on any class of issues - the ones they a representatives of).
        </p>
    </div>
    <p class="mt-3">@michael-liesenfelt (posted on November 27, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            ...it would be wise to limit the voting power of all SPO's to some leverage limit multiple of the SPO's pledge: L. This has a profound effect: there is an added social/governance motivation for pledge.
        </p>
    </div>
    <p class="mt-3">@Balance-Analytics (posted on November 27, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            On the topic of pledge and leverage, would it be possible to include time duration for pledge existence? The use of time duration for a pools pledge (i.e. 10 epochs) could prevent pools from updating their pledge to increase vote impact then remove the pledge to keep their ADA in a more liquid state.
        </p>
    </div>
    <p class="mt-3">@GGAlanSmithee (posted on November 26, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            "Requiring a large deposit when registering oneself as a DRep."... I'm not sure this is a great idea. I think it's a good idea in the case of SPOs, since your stake is directly involved in the method of securing the network, but in the case of dReps, that will be voting on issues regarding the future changes to the network, other factors, such as expertise in a given area is IMO more important.
        </p>
    </div>
    <p class="mt-3">@ia-davidpichler (posted on November 26, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            ...the US constitution doesn't lay out Congress customs and proceedings in it, just what they are responsible for.
        </p>
    </div>
    <p class="mt-3">@kronoshus (posted on November 26, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            If law is not created before the governance proposal is implemented, then there will be many issues in the future or people simply will not use the system, call it broken like what happened with Catalyst, and move on with their lives.
        </p>
    </div>
    <p class="mt-3">@kronoshus (posted on November 26, 2022)</p>
    <div class="mt-4 bg-slate-100 h 20 p-5 border-4  border-l-slate-400">
        <p>
            There is no legal procedure to defend oneself if being removed from a committee, thus what incentive would anyone have to do anything once on the committee besides hold power.
        </p>
    </div>
    <p class="mt-2">
        In general, concerns seem to focus on how to manage cliques that may develop within a governance model. The concerns are justified. At the most general level, a clique is a sub-set of a network in which the actors are more closely and intensely tied to one another than they are to other members of the network. While cliques may form as a natural tendency of human behaviour, cliques must be both manageable and managed to preserve a society or network as a whole. Changing too much at once in the Cardano ecosystem would increase risk, and may potentially lead to unforeseen—even catastrophic—consequences. A Sybil attack is an extreme example of clique behavior.
    </p>
    <p class="mt-2">
        As @ia-davidpichler seems to confirm in passing, CIP-1694? is based on a model of government fashioned after the United States. Cardano is a global ecosystem not necessarily bound by geopolitics.
        Cardano infrastructure that exists today has successfully shepherded the development and growth of Cardano as a top-ten project within the Web3 space for the past five or more years. I would suggest that the concerns listed above may be mitigated by leveraging global infrastructures already in place within the Cardano ecosystem.
        Also, arguably, decentralized autonomous organizations (DAOs) are the future of governance.
    </p>
    <p class="mt-2">
        Engaging organizations responsible for the creation of Cardano and respective <b>employees as governance ambassadors</b> leverages existing standards of professional conduct, working relationships, legal contracts, agreements, processes and arrangements already in place with individuals and groups already proven over time to be effective, with motivation to continue preserving and garnering the interests of the wider Cardano community.
        Individual employees able to engage with SPOs in the form of offering delegations empowers employees to incentivize stake pools, stake pool operators and developers to create and maintain projects that are not only in the interests of the project owners, but are also useful to the Cardano ecosystem as a whole.
    </p>
    <p class="mt-2">
        Incentivizing SPOs offers a means to incentivize prospective delegators indirectly as well, in order to increase overall market share and investment in Cardano over time.Cliques that may form between individuals and teams of employees are manageable by the employer at the middle and upper management levels.
    </p>
    <p class="mt-2">
        No one would be forced to be interested in the good graces of an employee DAO. <b>By introducing an employee DAO that shares a balance of power with respective employers, with the support of the community, a punitive system of governance for the Cardano community is not required any more than has been required over the past five or more years.</b>
        At this point, as a <b>sidebar in parallel to the current thread</b>, I would like to invite debate, thoughts and questions on the potential benefits of a DAO governance model for Cardano based on the draft proposal It Takes a Village. Please feel free to leave a comment or create an Issue.
    </p>
    <div class="flex items-center mt-6 mb-5">
        <p class="text-zinc-400 text-sm">
            2022-11-29T02:08:45Z
        </p>
        <a href="https://github.com/cardano-foundation/CIPs/pull/380#issuecomment-1329982080" target="_blank" class="ml-3 text-sm hover:bg-gray-100 p-1"> view on Github</a>
    </div>
</div>